#include <iostream>
#include <map>
#include <unistd.h>
#include "utf/WordReader.h"
#include "utf/CharacterReader.h"

void printWords(const char *patch) {
    auto reader = utf::WordReader(patch);
    std::string word;
    while (!(word = reader.getUTF8word()).empty()) {
        std::cout << word << std::endl;
    }
}

void printCharacter(const char *patch) {
    auto reader = utf::CharacterReader(patch);
    std::cout << reader.getLower() << std::endl;
}

int main() {
    auto patch = "../test.txt";
    if (access(patch, F_OK) == 0) {
        printWords(patch);
    } else {
        return 1;
    }
    return 0;
}
