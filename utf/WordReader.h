//
// Created by ghost on 16.10.2019.
//

#ifndef WORDSCOUNT_WORDREADER_H
#define WORDSCOUNT_WORDREADER_H

#include <unicode/unistr.h>
#include "CharacterReader.h"
#include <unicode/ustdio.h>

namespace utf {
    class WordReader {
    private:
        utf::CharacterReader reader{};
    public:
        WordReader() = default;

        explicit WordReader(const char *patch) {
            reader = CharacterReader(patch);
        }

        icu::UnicodeString get() {
            icu::UnicodeString word;
            UChar character;
            while ((character = reader.getLower()) != U_EOF) {
                if (utf::CharacterReader::isPunct(character)) {
                    continue;
                }
                if (utf::CharacterReader::isSpace(character)) {
                    if (word.isEmpty()) {
                        continue;
                    } else {
                        return word;
                    }
                }
                word.append(character);
            }

            return word;
        }

        std::string getUTF8word() {
            std::string wordUtf8;
            icu::UnicodeString word = get();
            word.toUTF8String(wordUtf8);
            return wordUtf8;
        };
    };
}

#endif //WORDSCOUNT_WORDREADER_H
