//
// Created by ghost on 15.10.2019.
//
#ifndef WORDSCOUNT_CHARACTERREADER_H
#define WORDSCOUNT_CHARACTERREADER_H

#include <unicode/ustdio.h>
#include <string>
#include <unicode/utypes.h>

namespace utf {
    class CharacterReader {
    private:
        UFILE *file;
    public:
        CharacterReader() = default;;

        explicit CharacterReader(const char *patch) {
            file = u_fopen(patch, "r", nullptr, nullptr);
            if (file == nullptr) {
                throw "Файл не открыт";
            }
        }

        ~CharacterReader() {
            if (file != nullptr) {
                u_fclose(file);
            }
        }

        UChar get() {
            return u_fgetc(file);
        }

        UChar getLower() {
            return u_tolower(get());
        }

        static bool isSpace(UChar character) {
            return u_isspace(character);
        }

        static bool isPunct(UChar character) {
            return u_ispunct(character);
        }
    };
}


#endif //WORDSCOUNT_CHARACTERREADER_H
